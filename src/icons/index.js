import AllIcon from "./AllIcon.vue";
import CalendarIcon from "./CalendarIcon.vue";
import CancelIcon from "./CancelIcon.vue";
import ChevronDown from "./ChevronDown.vue";
import ChevronLeftIcon from "./ChevronLeftIcon.vue";
import ChevronRightIcon from "./ChevronRightIcon.vue";
import CircleCrossIcon from "./CircleCrossIcon.vue";
import CircleTickIcon from "./CircleTickIcon.vue";
import ClockIcon from "./ClockIcon.vue";
import CloseIcon from "./CloseIcon.vue";
import CopyIcon from "./CopyIcon.vue";
import DeleteWarning from "./DeleteWarning.vue";
import DetailsIcon from "./DetailsIcon.vue";
import EditIcon from "./EditIcon.vue";
import EmptyImage from "./EmptyImage.vue";
import FileEarmarkImageIcon from "./FileEarmarkImageIcon.vue";
import FileEarmarkTextIcon from "./FileEarmarkTextIcon.vue";
import FlagIcon from "./FlagIcon.vue";
import Frame from "./Frame.vue";
import GroupColourFulIcon from "./GroupColourFulIcon.vue";
import GroupIcon from "./GroupIcon.vue";
import LabelIcon from "./LabelIcon.vue";
import LogoHome from "./Logo_white.vue";
import Logo from "./Logo.vue";
import MenuIcon from "./MenuIcon.vue";
import PencilIcon from "./PencilIcon.vue";
import PlayIcon from "./PlayIcon.vue";
import PlusIcon from "./PlusIcon.vue";
import PlusSquareIcon from "./PlusSquareIcon.vue";
import QRCodeIcon from "./QRCodeIcon.vue";
import QuestionMarkCircle from "./QuestionMarkCircle.vue";
import RemarkIcon from "./RemarkIcon.vue";
import RemoveCrossIcon from "./RemoveCrossIcon.vue";
import RequiredIcon from "./RequiredIcon.vue";
import SearchIcon from "./SearchIcon.vue";
import SearchIcon2 from "./SearchIcon2.vue";
import StopIcon from "./StopIcon.vue";
import ThreeDotsIcon from "./ThreeDotsIcon.vue";
import TickIcon from "./TickIcon.vue";
import TrashFillIcon from "./TrashFillIcon.vue";
import TypeIcon from "./TypeIcon.vue";
import UrlIcon from "./UrlIcon.vue";
import ValidDrop from "./ValidDrop.vue";
import Windows from "./Windows.vue";

export {
  AllIcon,
  CalendarIcon,
  CancelIcon,
  ChevronDown,
  ChevronLeftIcon,
  ChevronRightIcon,
  CircleCrossIcon,
  CircleTickIcon,
  ClockIcon,
  CloseIcon,
  CopyIcon,
  DeleteWarning,
  DetailsIcon,
  EditIcon,
  EmptyImage,
  FileEarmarkImageIcon,
  FileEarmarkTextIcon,
  FlagIcon,
  Frame,
  GroupColourFulIcon,
  GroupIcon,
  LabelIcon,
  LogoHome,
  Logo,
  MenuIcon,
  PencilIcon,
  PlayIcon,
  PlusIcon,
  PlusSquareIcon,
  QRCodeIcon,
  QuestionMarkCircle,
  RemarkIcon,
  RemoveCrossIcon,
  RequiredIcon,
  SearchIcon,
  SearchIcon2,
  StopIcon,
  ThreeDotsIcon,
  TickIcon,
  TrashFillIcon,
  TypeIcon,
  UrlIcon,
  ValidDrop,
  Windows
};
