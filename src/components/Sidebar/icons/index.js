import BoardcastIcon from "./BoardcastIcon.vue";
import ChevronDownIcon from "./ChevronDownIcon.vue";
import ChevronRightIcon from "./ChevronRightIcon.vue";
import LanguageIcon from "./LanguageIcon.vue";
import LinkIcon from "./LinkIcon.vue";
import LogoutIcon from "./LogoutIcon.vue";
import PermissionsIcon from "./PermissionsIcon.vue";
import SEOIcon from "./SEOIcon.vue";
import SettingIcon from "./SettingIcon.vue";
import SidebarCloseIcon from "./SidebarCloseIcon.vue";
import SidebarOpenIcon from "./SidebarOpenIcon.vue";
import ToolsIcon from "./ToolsIcon.vue";
import UserGroupsIcon from "./UserGroupsIcon.vue";
import UserSettingsIcon from "./UserSettingsIcon.vue";

export {
  BoardcastIcon,
  ChevronDownIcon,
  ChevronRightIcon,
  LanguageIcon,
  LinkIcon,
  LogoutIcon,
  PermissionsIcon,
  SEOIcon,
  SettingIcon,
  SidebarCloseIcon,
  SidebarOpenIcon,
  ToolsIcon,
  UserGroupsIcon,
  UserSettingsIcon,
};
