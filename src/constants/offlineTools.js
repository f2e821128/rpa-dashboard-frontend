// 這裡的 Create mode 和 Edit mode 原本想要坐成 Enum type
const CREATEMODE = "create";
const EDITMODE = "edit";

// 站群推播的分頁
const OFFLINE_TOOL_TAB = [
  { id: 105, translateKey: "offlinetools.tab.user" },
  { id: 106, translateKey: "offlinetools.tab.role" },
];

const OFFLINE_TOOL_CHILD = {
  USER: 105,
  RULE: 106,
};

const USERS_HEADER_CONFIG = [
  { text: "認證碼", value: "auth_code" },
  { text: "功能", value: "module_list" },
  { text: "機碼", value: "uuid" },
  { text: "啟用/停用", value: "active" },
  { text: "啟用時間", value: "activated_time" },
  { text: "檢查狀態", value: "valid" },
  { text: "檢查時間", value: "checked_time" },
  { text: "操作", value: "control" },
];

const RULES_HEADER_CONFIG = [
  { text: "功能", value: "module_name" },
  { text: "渠道", value: "submodule_name" },
];

export { CREATEMODE, EDITMODE, OFFLINE_TOOL_TAB, OFFLINE_TOOL_CHILD, USERS_HEADER_CONFIG, RULES_HEADER_CONFIG };
