const ACCOUNT_STATUS = {
  ACTIVE: 0,
  INACTIVE: -1,
};

const DRAWER_MODE = {
  CREATE: "create",
  DEATILS: "details",
  EDIT: "edit",
};

// Fake access token
const ACCESS_TOKEN = import.meta.env.VITE_FAKE_ACCESS_TOKEN;

export { ACCOUNT_STATUS, DRAWER_MODE, ACCESS_TOKEN };
