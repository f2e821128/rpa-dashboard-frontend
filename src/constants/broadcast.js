// 這裡的 Create mode 和 Edit mode 原本想要坐成 Enum type
const CREATEMODE = "create";
const EDITMODE = "edit";

// 站群推播的分頁
const BROADCAST_TAB = [
  { id: 73, translateKey: "broadcast.tab.list" },
  { id: 72, translateKey: "broadcast.tab.website" },
  { id: 71, translateKey: "broadcast.tab.labels" },
];

const BROADCAST_CHILD = {
  MISSION: 73,
  WEBSITE: 72,
  LABELS: 71,
};

// 站群推播成效分析 - 推播分析 - 的分頁
const BROADCAST_ANAL_DEETS_TAB = [
  { id: 1, translateKey: "broadcast.tab.pushAudience" },
  { id: 2, translateKey: "broadcast.tab.pushWebsite" },
  { id: 3, translateKey: "broadcast.tab.pushCampaignCreative" },
];

// 站群推播成效分析 - 會員成效 - 的分頁
// ESB:ESB會員成效 ISC:足球滾會員成效 FN:蜂鳥會員成效 ELINE:e賴會員成效
const BROADCAST_EFF_DEETS_TAB = [
  { id: "ESB", translateKey: "broadcast.tab.esbMem" },
  { id: "ISC", translateKey: "broadcast.tab.footballRollMem" },
  { id: "FN", translateKey: "broadcast.tab.hummingbirdMem" },
  { id: "ELINE", translateKey: "broadcast.tab.eLaiMem" },
];

// 標籤管理 Table 的 Table Header
const labelMgmtHeadersConfig = [
  { text: "代碼", value: "tag_code" },
  { text: "類型", value: "topic" },
  { text: "名稱", value: "tag_name" },
  { text: "建立人員", value: "user_name" },
  { text: "操作", value: "control" },
];

// 網站管理 Table 的 Table Header
const siteMgmtHeadersConfig = [
  { text: "名称", value: "sitename" },
  { text: "網址", value: "url" },
  { text: "地區", value: "region" },
  { text: "標籤", value: "tag_list" },
  { text: "操作", value: "control" },
];

// 網站管理 - 成效分析 Table 的 Table Header
const effectivenessAnalysisTotalVisitorHeader = [
  { text: "日期", value: "data_date" },
  { text: "造訪數", value: "visit_num", sortable: true },
  { text: "會員造訪數", value: "member_visit_num", sortable: true },
];

// 任務清單 Table 的 Table Header
const broadcastHeadersConfig = [
  { text: "狀態", value: "status" },
  { text: "名稱", value: "project_name" },
  { text: "類型", value: "type" },
  { text: "建立時間", value: "createdat" },
  { text: "建立人員", value: "creator" },
  { text: "操作", value: "control" },
];

// 任務清單 - 成效分析 Table 的 Table Header
const broadcastEffAnalHeadersConfig = [
  { text: "狀態", value: "status" },
  { text: "名稱", value: "project_name" },
  { text: "受眾數", value: "audience_num" },
  { text: "曝光數", value: "browse_num" },
  { text: "點擊數", value: "click_num" },
  { text: "曝光率", value: "browse_rate" },
  { text: "點擊率", value: "click_rate" },
  { text: "會員曝光數", value: "member_browse_num" },
  { text: "會員點擊數", value: "member_click_num" },
  { text: "建立人員", value: "creator" },
  { text: "操作", value: "control" },
];

export {
  CREATEMODE,
  EDITMODE,
  BROADCAST_TAB,
  BROADCAST_CHILD,
  BROADCAST_ANAL_DEETS_TAB,
  BROADCAST_EFF_DEETS_TAB,
  labelMgmtHeadersConfig,
  siteMgmtHeadersConfig,
  effectivenessAnalysisTotalVisitorHeader,
  broadcastHeadersConfig,
  broadcastEffAnalHeadersConfig,
};
