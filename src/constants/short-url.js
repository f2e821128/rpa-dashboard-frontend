// 這裡的 Create mode 和 Edit mode 原本想要坐成 Enum type
const CREATEMODE = "create";
const EDITMODE = "edit";

// 網址清單的類型分類
const typeConfig = {
  name: "fliter",
  default: "群組",
  data: ["群組", "名稱"],
};

// 短網址的分頁
const SHORT_URL_TAB = [
  { id: 88, translateKey: "shorturl.tab.list" },
  { id: 103, translateKey: "shorturl.tab.rule" },
];

const SHORT_URL_CHILD = {
  LIST: 88,
  RULE: 103,
};

const redirectRulesHeadersConfig = [
  { text: "組別", value: "group_name" },
  { text: "名稱", value: "name" },
  { text: "服務商", value: "service_name" },
  { text: "網域名", value: "domain" },
  { text: "跳轉頁", value: "redirect_link" },
  { text: "操作", value: "control" },
];

const urlListHeader = [
  { text: "网址", value: "shot_url" },
  { text: "状态", value: "short_link_status" },
  { text: "操作", value: "control" },
];

const effectivenessAnalysisTotalURLHeader = [
  { text: "短網址", value: "text" },
  { text: "點擊數", value: "sum_click_num", sortable: true },
  { text: "點擊人數", value: "sum_member_click_num", sortable: true },
];

const effectivenessAnalysisDailyURLHeader = [
  { text: "日期", value: "text" },
  { text: "點擊數", value: "sum_click_num", sortable: true },
  { text: "點擊人數", value: "sum_member_click_num", sortable: true },
];

const urlListItems = [
  { url: "https://alink.com", status: "有效", operation: "" },
  { url: "https://blink.com", status: "有效", operation: "" },
  { url: "https://clink.com", status: "有效", operation: "" },
  { url: "https://dlink.com", status: "有效", operation: "" },
  { url: "https://elink.com", status: "有效", operation: "" },
  { url: "https://flink.com", status: "有效", operation: "" },
  { url: "https://glink.com", status: "有效", operation: "" },
  { url: "https://hlink.com", status: "有效", operation: "" },
  { url: "https://ilink.com", status: "有效", operation: "" },
  { url: "https://jlink.com", status: "有效", operation: "" },
  { url: "https://klink.com", status: "有效", operation: "" },
  { url: "https://llink.com", status: "有效", operation: "" },
  { url: "https://mlink.com", status: "有效", operation: "" },
  { url: "https://nlink.com", status: "有效", operation: "" },
  { url: "https://olink.com", status: "有效", operation: "" },
  { url: "https://plink.com", status: "有效", operation: "" },
  { url: "https://qlink.com", status: "有效", operation: "" },
  { url: "https://rlink.com", status: "有效", operation: "" },
  { url: "https://slink.com", status: "有效", operation: "" },
  { url: "https://tlink.com", status: "有效", operation: "" },
  { url: "https://ulink.com", status: "有效", operation: "" },
  { url: "https://vlink.com", status: "有效", operation: "" },
  { url: "https://wlink.com", status: "有效", operation: "" },
  { url: "https://xlink.com", status: "有效", operation: "" },
  { url: "https://ylink.com", status: "有效", operation: "" },
  { url: "https://zlink.com", status: "有效", operation: "" },
];

export {
  CREATEMODE,
  EDITMODE,
  SHORT_URL_TAB,
  SHORT_URL_CHILD,
  typeConfig,
  redirectRulesHeadersConfig,
  urlListHeader,
  effectivenessAnalysisTotalURLHeader,
  effectivenessAnalysisDailyURLHeader,
  urlListItems,
};
