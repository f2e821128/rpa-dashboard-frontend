const langConfig = {
  default: "lang.tw",
  data: ["lang.tw", "lang.cn"],
};

/**
 * Sidebar define mapping
 * Broadcast => 79
 * Broadcast - MissionList => 73
 * Broadcast - WebsiteMgmt => 72
 * Broadcast - LabelMgmt => 71
 *
 * Short Link => 87
 * Short Link - WebsiteList => 88
 * Short Link - RedirectRules => 103
 *
 * System Setting => 43
 * User Mgmt => 45
 * Permission Mgmt => 46
 * Groups Mgmt => 82
 *
 * Now not available (Comming soon)
 * Offline Tools => 104
 */

// Current Available Service
const currentService = [79, 87, 104, 43];

// Available Sublist Service.
const sublist = [43];

const defaultActiveService = -1;

const SYSTEMSETTING = {
  USER: 45,
  PERMISSION: 46,
  GROUPS: 82,
};

export { langConfig, currentService, sublist, defaultActiveService, SYSTEMSETTING };
