import { createApp } from "vue";
import { createI18n } from "vue-i18n";
import { createPinia } from "pinia";
import router from "./router";

// Google Login
import GoogleSignInPlugin from "vue3-google-signin";

// Highcharts
import VueHighcharts from "vue3-highcharts";
import HighchartsVue from "highcharts-vue";

// Datatable
import Vue3EasyDataTable from "vue3-easy-data-table";
import "vue3-easy-data-table/dist/style.css";

// i18n library
import tw from "./locales/zh-tw.json";
import cn from "./locales/zh-cn.json";

import "./style.css";
import "./custom-loding.css";
import "./custom-multiselect.css";
import "./rpa_push.css";
import App from "./App.vue";

const app = createApp(App);

const i18n = createI18n({
  legacy: false,
  locale: "tw",
  messages: {
    tw,
    cn,
  },
});

app.use(GoogleSignInPlugin, {
  clientId: "558901848607-puje9oa0eu2bkdh5ljqpsmebv17dueqv.apps.googleusercontent.com",
  scope: "email profile",
});

const pinia = createPinia();

app.use(i18n);
app.use(pinia);
app.use(router);
app.use(VueHighcharts);
app.use(HighchartsVue);
app.component("EasyDataTable", Vue3EasyDataTable);

app.mount("#app");
