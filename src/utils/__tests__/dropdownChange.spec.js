// Test Example
import { describe, it, expect, beforeEach } from "vitest";
import { dropdownChange } from "../dropdownChange";

describe("Test dropdownChange function", () => {
  let mockObj = null;
  beforeEach(() => {
    mockObj = { default: "" };
  });

  it("initializes with empty string ", () => {
    expect(mockObj.default).toEqual("");
  });

  it("test today function", () => {
    dropdownChange(mockObj, "aaa");

    expect(mockObj.default).toEqual("aaa");
  });
});
