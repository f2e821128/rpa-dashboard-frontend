// Test Example
import { describe, it, expect, vi } from "vitest";
import { dayBefore, today } from "../countDate";
import jest from "jest";

describe("countDate Function Test", () => {
  it("test today function", () => {
    const result = today();

    const d = new Date();
    const mm = d.getMonth() + 1 < 10 ? `0${d.getMonth() + 1}` : `${d.getMonth() + 1}`;
    const dd = d.getDate() < 10 ? `0${d.getDate()}` : `${d.getDate()}`;

    expect(result).toEqual(`${d.getFullYear()}-${mm}-${dd}`);
  });

  it("test today function with mock after Oct", () => {
    const mockDate = new Date(2022, 10, 1);
    vi.setSystemTime(mockDate);
    const result = today();

    const d = new Date();
    const mm = d.getMonth() + 1 < 10 ? `0${d.getMonth() + 1}` : `${d.getMonth() + 1}`;
    const dd = d.getDate() < 10 ? `0${d.getDate()}` : `${d.getDate()}`;

    expect(result).toEqual(`${d.getFullYear()}-${mm}-${dd}`);
  });

  it("test today function with mock before Oct", () => {
    const mockDate = new Date(2022, 2, 1);
    vi.setSystemTime(mockDate);
    const result = today();

    const d = new Date();
    const mm = d.getMonth() + 1 < 10 ? `0${d.getMonth() + 1}` : `${d.getMonth() + 1}`;
    const dd = d.getDate() < 10 ? `0${d.getDate()}` : `${d.getDate()}`;

    expect(result).toEqual(`${d.getFullYear()}-${mm}-${dd}`);
  });

  it("test dayBefore function 2 week before", () => {
    const fewDaysBefore = 14;
    const result = dayBefore(fewDaysBefore);

    const d = new Date();
    const previous = new Date(d.getTime());
    previous.setDate(d.getDate() - fewDaysBefore + 1);
    const mm = previous.getMonth() + 1 < 10 ? `0${previous.getMonth() + 1}` : `${previous.getMonth() + 1}`;
    const dd = previous.getDate() < 10 ? `0${previous.getDate()}` : `${previous.getDate()}`;

    expect(result).toEqual(`${previous.getFullYear()}-${mm}-${dd}`);
  });

  it("test dayBefore function 1 month before", () => {
    const fewDaysBefore = 30;
    const result = dayBefore(fewDaysBefore);

    const d = new Date();
    const previous = new Date(d.getTime());
    previous.setDate(d.getDate() - fewDaysBefore + 1);
    const mm = previous.getMonth() + 1 < 10 ? `0${previous.getMonth() + 1}` : `${previous.getMonth() + 1}`;
    const dd = previous.getDate() < 10 ? `0${previous.getDate()}` : `${previous.getDate()}`;

    expect(result).toEqual(`${previous.getFullYear()}-${mm}-${dd}`);
  });

  it("test dayBefore function 3 month before", () => {
    const fewDaysBefore = 90;
    const result = dayBefore(fewDaysBefore);

    const d = new Date();
    const previous = new Date(d.getTime());
    previous.setDate(d.getDate() - fewDaysBefore + 1);
    const mm = previous.getMonth() + 1 < 10 ? `0${previous.getMonth() + 1}` : `${previous.getMonth() + 1}`;
    const dd = previous.getDate() < 10 ? `0${previous.getDate()}` : `${previous.getDate()}`;

    expect(result).toEqual(`${previous.getFullYear()}-${mm}-${dd}`);
  });

  it("test dayBefore function 6 month before", () => {
    const fewDaysBefore = 180;
    const result = dayBefore(fewDaysBefore);

    const d = new Date();
    const previous = new Date(d.getTime());
    previous.setDate(d.getDate() - fewDaysBefore + 1);
    const mm = previous.getMonth() + 1 < 10 ? `0${previous.getMonth() + 1}` : `${previous.getMonth() + 1}`;
    const dd = previous.getDate() < 10 ? `0${previous.getDate()}` : `${previous.getDate()}`;

    expect(result).toEqual(`${previous.getFullYear()}-${mm}-${dd}`);
  });

  it("test dayBefore function 9 month before", () => {
    const fewDaysBefore = 270;
    const result = dayBefore(fewDaysBefore);

    const d = new Date();
    const previous = new Date(d.getTime());
    previous.setDate(d.getDate() - fewDaysBefore + 1);
    const mm = previous.getMonth() + 1 < 10 ? `0${previous.getMonth() + 1}` : `${previous.getMonth() + 1}`;
    const dd = previous.getDate() < 10 ? `0${previous.getDate()}` : `${previous.getDate()}`;

    expect(result).toEqual(`${previous.getFullYear()}-${mm}-${dd}`);
  });
});
