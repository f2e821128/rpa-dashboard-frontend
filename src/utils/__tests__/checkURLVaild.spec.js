// Test Example
import { describe, it, expect } from "vitest";
import isValidUrl from "../checkURLVaild";

describe("URL Vaild Test", () => {
  it("check URL vaild", () => {
    const result = isValidUrl("https://baidu.com");
    expect(result).toEqual(true);
  });

  it("check URL invaild without https", () => {
    const result = isValidUrl("baidu.com");
    expect(result).toEqual(false);
  });

  it("check URL invaild without https", () => {
    const result = isValidUrl("baidu");
    expect(result).toEqual(false);
  });

  it("check URL invaild without https", () => {
    const result = isValidUrl("www.baidu");
    expect(result).toEqual(false);
  });

  it("check URL invaild without https", () => {
    const result = isValidUrl("www.baidu.com");
    expect(result).toEqual(false);
  });
});
