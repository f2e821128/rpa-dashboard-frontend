import { dispatchNotification } from "../components/Notification";

const copyNotification = (content) => {
  dispatchNotification({ title: "URL Copied", content: content, type: "" });
};

const copyClipboard = (url) => {
  copyNotification(url);
  navigator.clipboard.writeText(url);
};

export default copyClipboard;
