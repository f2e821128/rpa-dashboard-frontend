const today = () => {
  const d = new Date();
  const mm = d.getMonth() + 1 < 10 ? `0${d.getMonth() + 1}` : `${d.getMonth() + 1}`;
  const dd = d.getDate() < 10 ? `0${d.getDate()}` : `${d.getDate()}`;

  return `${d.getFullYear()}-${mm}-${dd}`;
};

const dayBefore = (num) => {
  const d = new Date();
  const previous = new Date(d.getTime());
  previous.setDate(d.getDate() - num + 1);
  const mm = previous.getMonth() + 1 < 10 ? `0${previous.getMonth() + 1}` : `${previous.getMonth() + 1}`;
  const dd = previous.getDate() < 10 ? `0${previous.getDate()}` : `${previous.getDate()}`;

  return `${previous.getFullYear()}-${mm}-${dd}`;
};

export { dayBefore, today };
