import { errorReport } from "../helpers/query-api";

export const logging = async (errObject, errMode) => {
  // 類型 "1":error "2":info "3":debug
  let errType = errMode === "error" ? "1" : errMode === "info" ? "2" : "3";
  await errorReport({
    content: JSON.stringify(errObject),
    type: errType,
  });
};
