import { defineStore } from "pinia";

export const useSidebarStore = defineStore("sidebar", {
  state: () => ({
    active: "",
    subActive: "",
    permission: [],
  }),
  getters: {
    getActive() {
      return this.active;
    },
    getSubActive() {
      return this.subActive;
    },
    getPermissionStatus() {
      return this.permission;
    },
  },
  actions: {
    updatePermissions(state) {
      this.permission.push(state);
    },
    updateSidebarActive(state) {
      this.active = state;
    },
  },
});
