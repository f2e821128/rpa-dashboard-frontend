import { defineStore } from "pinia";

export const useLoadingStore = defineStore("loading", {
  state: () => ({
    show: false,
  }),
  getters: {
    getStatus() {
      return this.show;
    },
  },
  actions: {
    off() {
      this.show = false;
    },
    on() {
      this.show = true;
    },
  },
});
