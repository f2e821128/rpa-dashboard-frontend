// Test Example
import { describe, it, expect, beforeEach } from "vitest";
import { setActivePinia, createPinia } from "pinia";
import { useCounterStore } from "../useCounterStore";

describe("Counter Store Test", () => {
  let store = null;
  
  beforeEach(() => {
    // create a fresh Pinia instance and make it active so it's automatically picked
    // up by any useStore() call without having to pass it to it:
    // `useStore(pinia)`
    setActivePinia(createPinia());

    // create an instance of the data store
    store = useCounterStore();
  });

  it("initializes with zero ", () => {
    expect(store.double).toEqual(0);
  });

  it("test increment state", () => {
    // Call the 'increment' action
    store.increment();

    // Check that the double result
    expect(store.double).toEqual(2);
  });
}) 