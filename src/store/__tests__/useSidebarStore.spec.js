// Test Example
import { describe, it, expect, beforeEach } from "vitest";
import { setActivePinia, createPinia } from "pinia";
import { useSidebarStore } from "../useSidebarStore";

describe("Sidebar Store Test", () => {
  let store = null;

  beforeEach(() => {
    // create a fresh Pinia instance and make it active so it's automatically picked
    // up by any useStore() call without having to pass it to it:
    // `useStore(pinia)`
    setActivePinia(createPinia());

    // create an instance of the data store
    store = useSidebarStore();
  });

  it("initializes active status with empty string", () => {
    expect(store.getActive).toEqual("");
  });

  it("initializes permission status with empty array", () => {
    expect(store.getPermissionStatus).toEqual([]);
  });

  it("initializes permission status with length 0", () => {
    expect(store.getPermissionStatus.length).toEqual(0);
  });

  it("test updatePermissions actions", () => {
    // Call the 'updatePermissions' action
    store.updatePermissions("1");
    store.updatePermissions("2");

    // Check that the getPermissionStatus result
    expect(store.getPermissionStatus[0]).toMatch("1");
    expect(store.getPermissionStatus.length).toEqual(2);
  });

  it("test updateSidebarActive actions with single action", () => {
    // Call the 'updateSidebarActive' action
    store.updateSidebarActive("1");

    // Check that the getActive result
    expect(store.getActive).toMatch("1");
    expect(store.getActive).toEqual("1");
  });

  it("test updateSidebarActive actions with Multiple action", () => {
    // Call the 'updateSidebarActive' action
    store.updateSidebarActive("1");

    // Check that the getActive result
    expect(store.getActive).toMatch("1");
    expect(store.getActive).toEqual("1");

    // Call the 'updateSidebarActive' action
    store.updateSidebarActive("10");

    // Check that the getActive result
    expect(store.getActive).toMatch("10");
    expect(store.getActive).toEqual("10");
  });
});
