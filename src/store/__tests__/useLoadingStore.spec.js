// Test Example
import { describe, it, expect, beforeEach } from "vitest";
import { setActivePinia, createPinia } from "pinia";
import { useLoadingStore } from "../useLoadingStore";

describe("Loading Store Test", () => {
  let store = null;
  
  beforeEach(() => {
    // create a fresh Pinia instance and make it active so it's automatically picked
    // up by any useStore() call without having to pass it to it:
    // `useStore(pinia)`
    setActivePinia(createPinia());

    // create an instance of the data store
    store = useLoadingStore();
  });

  it("initializes loading status with false", () => {
    expect(store.getStatus).toEqual(false);
  });

  it("test loading on action", () => {
    // Call the 'on' action
    store.on();

    // Check that the getStatus status
    expect(store.getStatus).toEqual(true);
  });

  it("test loading on/off action", () => {
    // Call the 'on' action
    store.on();

    // Check that the getStatus status
    expect(store.getStatus).toEqual(true);

    // Call the 'on' action
    store.off();

    // Check that the getStatus status
    expect(store.getStatus).toEqual(false);
  });
}) 