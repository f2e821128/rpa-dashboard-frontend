// Test Example
import { describe, it, expect, beforeEach, beforeAll, afterAll } from "vitest";
// import {
//   createNewKeyMapping,
//   createQRcode,
//   createShortURL,
//   createGroups,
//   checkNameRepeat,
//   deleteKeyMapping,
//   deleteGroup,
//   updateShortURLDetails,
//   updateShortURLRemark,
//   updateGroups,
//   getShortURLDetails,
//   getShortURLList,
//   getShortURLDetailsInfo,
//   getShortURLRemark,
//   getRedirectRules,
//   getGroupListByUser,
//   getAvailableGroups,
//   regenerateShortURL,
// } from "../shortUrlAPI";
// import { refreshToken } from "../query-api";
// import { ACCESS_TOKEN } from "../../constants/system";

describe("short url api comming soon", () => {
  it("fake unit test", async () => {
    expect(1 + 1).toEqual(2);
  });
});

// 因為後端 API 出現問題無法進行整合測試
// describe("short url api test with 401 Unauthorized", () => {
//   beforeEach(() => {
//     Object.defineProperty(window, "location", {
//       value: {
//         href: "example.com",
//       },
//       writable: true,
//     });
//   });

//   it("createNewKeyMapping test", async () => {
//     let bodyFormData = new FormData();
//     const result = await createNewKeyMapping(bodyFormData);
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("createQRcode test", async () => {
//     const result = await createQRcode({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("createShortURL test", async () => {
//     const result = await createShortURL({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("createGroups test", async () => {
//     const result = await createGroups({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("checkNameRepeat test", async () => {
//     const result = await checkNameRepeat({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("deleteKeyMapping test", async () => {
//     const result = await deleteKeyMapping({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("deleteGroup test", async () => {
//     const result = await deleteGroup({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("updateShortURLDetails test", async () => {
//     const result = await updateShortURLDetails({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("updateShortURLRemark test", async () => {
//     const result = await updateShortURLRemark({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("updateGroups test", async () => {
//     const result = await updateGroups({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getShortURLDetails test", async () => {
//     const result = await getShortURLDetails({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getShortURLList test", async () => {
//     const result = await getShortURLList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getShortURLDetailsInfo test", async () => {
//     const result = await getShortURLDetailsInfo({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getShortURLRemark test", async () => {
//     const result = await getShortURLRemark({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getRedirectRules test", async () => {
//     const result = await getRedirectRules({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getGroupListByUser test", async () => {
//     const result = await getGroupListByUser({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getAvailableGroups test", async () => {
//     const result = await getAvailableGroups({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("regenerateShortURL test", async () => {
//     const result = await regenerateShortURL({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });
// });

// describe("redirect rules api with refresh token test", () => {
//   beforeAll(async () => {
//     // Mock access token into session storage.
//     Object.defineProperty(window, "sessionStorage", {
//       value: {
//         access_token: `${ACCESS_TOKEN}`,
//       },
//       writable: true,
//     });

//     const result = await refreshToken();

//     // update refresh access token into session storage.
//     window.sessionStorage.access_token = `${result.data.result.new_access_token}`;
//   });

//   afterAll(() => {
//     window.sessionStorage.access_token = "";
//   });

//   it("createQRcode test", async () => {
//     const response = await createQRcode({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("createShortURL test", async () => {
//     const response = await createShortURL({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//     expect(response).toHaveProperty("data.short_url_list");
//     expect(response).toHaveProperty("data.num");
//   });

//   it("createGroups test", async () => {
//     const response = await createGroups({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("checkNameRepeat test", async () => {
//     const response = await checkNameRepeat({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("deleteKeyMapping test", async () => {
//     const response = await deleteKeyMapping({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("deleteGroup test", async () => {
//     const response = await deleteGroup({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("updateShortURLDetails test", async () => {
//     const response = await updateShortURLDetails({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("updateShortURLRemark test", async () => {
//     const response = await updateShortURLRemark({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("updateGroups test", async () => {
//     const response = await updateGroups({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getShortURLDetails test", async () => {
//     const response = await getShortURLDetails({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//     expect(response).toHaveProperty("data.result.scenario_id", "");
//     expect(response).toHaveProperty("data.result.type", 0);
//     expect(response).toHaveProperty("data.result.name", "");
//     expect(response).toHaveProperty("data.result.encode_type", 0);
//     expect(response).toHaveProperty("data.result.redirect_link2", "");
//     expect(response).toHaveProperty("data.result.utm_source", "");
//     expect(response).toHaveProperty("data.result.utm_medium", "");
//     expect(response).toHaveProperty("data.result.utm_campaign", "");
//     expect(response).toHaveProperty("data.result.utm_term", "");
//     expect(response).toHaveProperty("data.result.utm_content", "");
//     expect(response).toHaveProperty("data.result.logo_img", false);
//     expect(response).toHaveProperty("data.result.mark", "");
//     expect(response).toHaveProperty("data.result.num", 0);
//     expect(response).toHaveProperty("data.result.short_url_num", 0);
//   });

//   it("getShortURLList test", async () => {
//     const response = await getShortURLList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result", []);
//     expect(response).toHaveProperty("data.num", 0);
//     expect(response.data.result).toHaveLength(0);
//   });

//   it("getShortURLDetailsInfo test", async () => {
//     const response = await getShortURLDetailsInfo({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result", []);
//     expect(response).toHaveProperty("data.num", 0);
//     expect(response.data.result).toHaveLength(0);
//   });

//   it("getShortURLRemark test", async () => {
//     const response = await getShortURLRemark({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result", "");
//   });

//   it("getRedirectRules test", async () => {
//     const response = await getRedirectRules({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result", "");
//     expect(response.data.result).not.toHaveLength(0);
//   });

//   it("getGroupListByUser test", async () => {
//     const response = await getGroupListByUser({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getAvailableGroups test", async () => {
//     const response = await getAvailableGroups({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("regenerateShortURL test", async () => {
//     const response = await regenerateShortURL({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });
// });
