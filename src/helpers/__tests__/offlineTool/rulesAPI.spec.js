// Test Example
import { describe, it, expect, beforeEach, beforeAll, afterAll } from "vitest";
// import { otCreateRule, otDeleteRule, otGetRuleList, otGetOnlyModuleData } from "../../offlineTool/rulesAPI";

describe("offline tool platform api comming soon", () => {
  it("fake unit test", async () => {
    expect(1 + 1).toEqual(2);
  });
});

// 因為後端 API 出現問題無法進行整合測試
// describe("offline tool platform api test", () => {
//   beforeEach(() => {
//     Object.defineProperty(window, "location", {
//       value: {
//         href: "example.com",
//       },
//       writable: true,
//     });
//   });

//   it("otCreateRule test", async () => {
//     const result = await otCreateRule({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("otDeleteRule test", async () => {
//     const result = await otDeleteRule({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("otGetRuleList test", async () => {
//     const result = await otGetRuleList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("otGetOnlyModuleData test", async () => {
//     const result = await otGetOnlyModuleData({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });
// });
