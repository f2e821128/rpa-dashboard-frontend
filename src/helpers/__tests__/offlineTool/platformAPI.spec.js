// Test Example
import { describe, it, expect, beforeEach, beforeAll, afterAll } from "vitest";
// import { otCreateUser, otUpdateUserStatus, otGetUserList, otGetUserEditData, otUpdateUserEditData, otGetModuleDropData } from "../../offlineTool/platformAPI";

describe("offline tool platform api test comming soon", () => {
  it("fake unit test", async () => {
    expect(1 + 1).toEqual(2);
  });
});

// 因為後端 API 出現問題無法進行整合測試
// describe("offline tool platform api test", () => {
//   beforeEach(() => {
//     Object.defineProperty(window, "location", {
//       value: {
//         href: "example.com",
//       },
//       writable: true,
//     });
//   });

//   it("otCreateUser test", async () => {
//     const result = await otCreateUser({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("otUpdateUserStatus test", async () => {
//     const result = await otUpdateUserStatus({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("otGetUserList test", async () => {
//     const result = await otGetUserList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("otGetUserEditData test", async () => {
//     const result = await otGetUserEditData({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("otUpdateUserEditData test", async () => {
//     const result = await otUpdateUserEditData({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("otGetModuleDropData test", async () => {
//     const result = await otGetModuleDropData({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });
// });
