// Test Example
import { describe, it, expect, beforeEach, beforeAll, afterAll } from "vitest";
// import {
//   createSite,
//   checkSiteName,
//   deleteSite,
//   updateSiteInfo,
//   updateSiteGroup,
//   getDomainList,
//   getEntmtCityList,
//   getBroadcastGroupList,
//   getSiteList,
//   getWebsiteData,
//   getGroupList,
//   geteffectivenessAnalysis,
//   getWebsiteGroupData,
//   getWebsiteGroupList,
//   getWebsiteList,
//   getSiteListByGroup,
//   getTagList,
// } from "../../broadcast/siteMgmtAPI";
// import { refreshToken } from "../../query-api";
// import { ACCESS_TOKEN } from "../../../constants/system";

describe("site Management api comming soon", () => {
  it("fake unit test", async () => {
    expect(1 + 1).toEqual(2);
  });
});

// 因為後端 API 出現問題無法進行整合測試
// describe("site Management api test", () => {
//   beforeEach(() => {
//     Object.defineProperty(window, "location", {
//       value: {
//         href: "example.com",
//       },
//       writable: true,
//     });
//   });

//   it("createSite test", async () => {
//     const result = await createSite({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("checkSiteName test", async () => {
//     const result = await checkSiteName({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("deleteSite test", async () => {
//     const result = await deleteSite({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("updateSiteInfo test", async () => {
//     const result = await updateSiteInfo({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("updateSiteGroup test", async () => {
//     const result = await updateSiteGroup({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getDomainList test", async () => {
//     const result = await getDomainList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getEntmtCityList test", async () => {
//     const result = await getEntmtCityList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getBroadcastGroupList test", async () => {
//     const result = await getBroadcastGroupList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getSiteList test", async () => {
//     const result = await getSiteList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getWebsiteData test", async () => {
//     const result = await getWebsiteData({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getGroupList test", async () => {
//     const result = await getGroupList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("geteffectivenessAnalysis test", async () => {
//     const result = await geteffectivenessAnalysis({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getWebsiteGroupData test", async () => {
//     const result = await getWebsiteGroupData({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getWebsiteGroupList test", async () => {
//     const result = await getWebsiteGroupList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getWebsiteList test", async () => {
//     const result = await getWebsiteList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getSiteListByGroup test", async () => {
//     const result = await getSiteListByGroup({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getTagList test", async () => {
//     const result = await getTagList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });
// });

// describe("site Management api with refresh token test", () => {
//   beforeAll(async () => {
//     // Mock access token into session storage.
//     Object.defineProperty(window, "sessionStorage", {
//       value: {
//         access_token: `${ACCESS_TOKEN}`,
//       },
//       writable: true,
//     });

//     const result = await refreshToken();

//     // update refresh access token into session storage.
//     window.sessionStorage.access_token = `${result.data.result.new_access_token}`;
//   });

//   afterAll(() => {
//     window.sessionStorage.access_token = "";
//   });

//   it("createSite test", async () => {
//     const response = await createSite({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("checkSiteName test", async () => {
//     const response = await checkSiteName({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("deleteSite test", async () => {
//     const response = await deleteSite({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("updateSiteInfo test", async () => {
//     const response = await updateSiteInfo({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("updateSiteGroup test", async () => {
//     const response = await updateSiteGroup({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getDomainList test", async () => {
//     const response = await getDomainList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getEntmtCityList test", async () => {
//     const response = await getEntmtCityList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getBroadcastGroupList test", async () => {
//     const response = await getBroadcastGroupList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getSiteList test", async () => {
//     const response = await getSiteList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getWebsiteData test", async () => {
//     const response = await getWebsiteData({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getGroupList test", async () => {
//     const response = await getGroupList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("geteffectivenessAnalysis test", async () => {
//     const response = await geteffectivenessAnalysis({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getWebsiteGroupData test", async () => {
//     const response = await getWebsiteGroupData({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getWebsiteGroupList test", async () => {
//     const response = await getWebsiteGroupList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getWebsiteList test", async () => {
//     const response = await getWebsiteList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getSiteListByGroup test", async () => {
//     const response = await getSiteListByGroup({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getTagList test", async () => {
//     const response = await getTagList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });
// });
