// Test Example
import { describe, it, expect, beforeEach, beforeAll, afterAll } from "vitest";
// import { createLabelTag, checkNewLabelTag, deleteLabelTag, getLabelTag, getLabelTagType } from "../../broadcast/labelsMgmtAPI";
// import { refreshToken } from "../../query-api";
// import { ACCESS_TOKEN } from "../../../constants/system";

describe("broadcast label management api comming soon", () => {
  it("fake unit test", async () => {
    expect(1 + 1).toEqual(2);
  });
});

// 因為後端 API 出現問題無法進行整合測試
// describe("broadcast label management api test", () => {
//   beforeEach(() => {
//     Object.defineProperty(window, "location", {
//       value: {
//         href: "example.com",
//       },
//       writable: true,
//     });
//   });

//   it("createLabelTag test", async () => {
//     const result = await createLabelTag({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("checkNewLabelTag test", async () => {
//     const result = await checkNewLabelTag({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("deleteLabelTag test", async () => {
//     const result = await deleteLabelTag({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getLabelTag test", async () => {
//     const result = await getLabelTag({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getLabelTagType test", async () => {
//     const result = await getLabelTagType({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });
// });

// describe("broadcast label management api with refresh token test", () => {
//   beforeAll(async () => {
//     // Mock access token into session storage.
//     Object.defineProperty(window, "sessionStorage", {
//       value: {
//         access_token: `${ACCESS_TOKEN}`,
//       },
//       writable: true,
//     });

//     const result = await refreshToken();

//     // update refresh access token into session storage.
//     window.sessionStorage.access_token = `${result.data.result.new_access_token}`;
//   });

//   afterAll(() => {
//     window.sessionStorage.access_token = "";
//   });

//   it("createLabelTag test", async () => {
//     const response = await createLabelTag({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("checkNewLabelTag test", async () => {
//     const response = await checkNewLabelTag({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("deleteLabelTag test", async () => {
//     const response = await deleteLabelTag({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getLabelTag test", async () => {
//     const response = await getLabelTag({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getLabelTagType test", async () => {
//     const response = await getLabelTagType({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });
// });
