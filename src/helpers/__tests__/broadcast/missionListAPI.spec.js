// Test Example
import { describe, it, expect, beforeEach, beforeAll, afterAll } from "vitest";
// import {
//   createSite,
//   checkBeginTimeRepeat,
//   checkJobRepeat,
//   deleteMissionData,
//   updateMissionData,
//   uploadImage,
//   getMissionData,
//   getEntmtCityLists,
//   getSiteMemberList,
//   getMisiionInfo,
//   getMissionDataList,
//   getAnalChartData,
//   getBroadcastSheetData,
//   getBroadcastSiteData,
//   getEventAdsData,
//   getMemberEff,
//   getEffAnalMissionData,
//   getEffAnalList,
//   getMissionList,
//   getBroadcastLabels,
// } from "../../broadcast/missionListAPI";
// import { refreshToken } from "../../query-api";
// import { ACCESS_TOKEN } from "../../../constants/system";

describe("broadcast mission list api comming soon", () => {
  it("fake unit test", async () => {
    expect(1 + 1).toEqual(2);
  });
});

// 因為後端 API 出現問題無法進行整合測試
// describe("broadcast mission list api test", () => {
//   beforeEach(() => {
//     Object.defineProperty(window, "location", {
//       value: {
//         href: "example.com",
//       },
//       writable: true,
//     });
//   });

//   it("createSite test", async () => {
//     const result = await createSite({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("checkBeginTimeRepeat test", async () => {
//     const result = await checkBeginTimeRepeat({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("checkJobRepeat test", async () => {
//     const result = await checkJobRepeat({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("deleteMissionData test", async () => {
//     const result = await deleteMissionData({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("updateMissionData test", async () => {
//     const result = await updateMissionData({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("uploadImage test", async () => {
//     const result = await uploadImage({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getMissionData test", async () => {
//     const result = await getMissionData({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getEntmtCityLists test", async () => {
//     const result = await getEntmtCityLists({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getSiteMemberList test", async () => {
//     const result = await getSiteMemberList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getMisiionInfo test", async () => {
//     const result = await getMisiionInfo({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getMissionDataList test", async () => {
//     const result = await getMissionDataList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getAnalChartData test", async () => {
//     const result = await getAnalChartData({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getBroadcastSheetData test", async () => {
//     const result = await getBroadcastSheetData({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getBroadcastSiteData test", async () => {
//     const result = await getBroadcastSiteData({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getEventAdsData test", async () => {
//     const result = await getEventAdsData({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getEffAnalMissionData test", async () => {
//     const result = await getEffAnalMissionData({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getEffAnalList test", async () => {
//     const result = await getEffAnalList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getMissionList test", async () => {
//     const result = await getMissionList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getBroadcastLabels test", async () => {
//     const result = await getBroadcastLabels({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });
// });

// describe("broadcast mission list api with refresh token test", () => {
//   beforeAll(async () => {
//     // Mock access token into session storage.
//     Object.defineProperty(window, "sessionStorage", {
//       value: {
//         access_token: `${ACCESS_TOKEN}`,
//       },
//       writable: true,
//     });

//     const result = await refreshToken();

//     // update refresh access token into session storage.
//     window.sessionStorage.access_token = `${result.data.result.new_access_token}`;
//   });

//   afterAll(() => {
//     window.sessionStorage.access_token = "";
//   });

//   it("createSite test", async () => {
//     const response = await createSite({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("checkBeginTimeRepeat test", async () => {
//     const response = await checkBeginTimeRepeat({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("checkJobRepeat test", async () => {
//     const response = await checkJobRepeat({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("deleteMissionData test", async () => {
//     const response = await deleteMissionData({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("updateMissionData test", async () => {
//     const response = await updateMissionData({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("uploadImage test", async () => {
//     const response = await uploadImage({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getMissionData test", async () => {
//     const response = await getMissionData({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getEntmtCityLists test", async () => {
//     const response = await getEntmtCityLists({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getSiteMemberList test", async () => {
//     const response = await getSiteMemberList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getMisiionInfo test", async () => {
//     const response = await getMisiionInfo({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getMissionDataList test", async () => {
//     const response = await getMissionDataList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getAnalChartData test", async () => {
//     const response = await getAnalChartData({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getBroadcastSheetData test", async () => {
//     const response = await getBroadcastSheetData({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getBroadcastSiteData test", async () => {
//     const response = await getBroadcastSiteData({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getEventAdsData test", async () => {
//     const response = await getEventAdsData({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getMemberEff test", async () => {
//     const response = await getMemberEff({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getEffAnalMissionData test", async () => {
//     const response = await getEffAnalMissionData({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getEffAnalList test", async () => {
//     const response = await getEffAnalList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getMissionList test", async () => {
//     const response = await getMissionList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getBroadcastLabels test", async () => {
//     const response = await getBroadcastLabels({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });
// });
