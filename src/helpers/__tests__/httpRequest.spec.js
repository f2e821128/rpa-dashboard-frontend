// Test Example
import { describe, it, expect, beforeEach } from "vitest";
// import { request, formDataRequest } from "../httpRequest";

describe("http request function test comming soon", () => {
  it("fake unit test", async () => {
    expect(1 + 1).toEqual(2);
  });
});

// 因為後端 API 出現問題無法進行整合測試
// describe("http request function test", () => {
//   beforeEach(() => {
//     Object.defineProperty(window, "location", {
//       value: {
//         href: "example.com",
//       },
//       writable: true,
//     });
//   });

//   it("request POST test", async () => {
//     const result = await request("POST", "api/other/log/frontend_log", {});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("request GET test", async () => {
//     const result = await request("GET", "api/other/log/frontend_log", {});
//     expect(result).toEqual("Error 404 Not Found");
//   });

//   it("request DELETE test", async () => {
//     const result = await request("DELETE", "api/other/log/frontend_log", {});
//     expect(result).toEqual("Error 404 Not Found");
//   });

//   it("request PUT test", async () => {
//     const result = await request("PUT", "api/other/log/frontend_log", {});
//     expect(result).toEqual("Error 404 Not Found");
//   });

//   it("request PATCH test", async () => {
//     const result = await request("PATCH", "api/other/log/frontend_log", {});
//     expect(result).toEqual("Error 404 Not Found");
//   });

//   it("request OPTIONS test", async () => {
//     const result = await request("OPTIONS", "api/other/log/frontend_log", {});
//     expect(result).toEqual(false);
//   });

//   it("formDataRequest POST test", async () => {
//     const result = await formDataRequest("POST", "api/other/log/frontend_log", {});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("formDataRequest OPTIONS test", async () => {
//     const result = await formDataRequest("OPTIONS", "api/other/log/frontend_log", {});
//     expect(result).toEqual(false);
//   });
// });
