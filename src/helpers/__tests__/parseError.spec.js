import { describe, it, expect } from "vitest";
// import parseError from "../parseError";

describe("parse error function test comming soon", () => {
  it("fake unit test", async () => {
    expect(1 + 1).toEqual(2);
  });
});

// 因為後端 API 出現問題無法進行整合測試
// describe("parse error function test", () => {
//   it("parse error with empty array test", () => {
//     let msg = [];
//     parseError(msg).catch((err) => expect(err).toEqual({ messages: [] }));
//   });
//   it("parse error with not array test", () => {
//     let msg = {};
//     parseError(msg).catch((err) => expect(err).toEqual({ messages: [{}] }));
//   });
//   it("parse error with nothing test", () => {
//     parseError().catch((err) => expect(err).toEqual({ messages: ["Error"] }));
//   });
// });
