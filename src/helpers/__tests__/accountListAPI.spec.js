// Test Example
import { describe, it, expect, beforeEach, beforeAll, afterAll } from "vitest";
// import {
//   createUser,
//   updateAccStatus,
//   updateAccDetails,
//   getRoleRealTime,
//   queryDefaultWindow,
//   queryEditGroup,
//   queryLeaderList,
//   queryUserPermission,
//   queryEditUserPermission,
//   queryUserInfo,
//   queryUserList,
// } from "../accountListAPI";
// import { refreshToken } from "../query-api";
// import { ACCESS_TOKEN } from "../../constants/system";

describe("account list api comming soon", () => {
  it("fake unit test", async () => {
    expect(1 + 1).toEqual(2);
  });
});

// 因為後端 API 出現問題無法進行整合測試
// describe("account list api test with 401 Unauthorized", () => {
//   beforeEach(() => {
//     Object.defineProperty(window, "location", {
//       value: {
//         href: "example.com",
//       },
//       writable: true,
//     });
//   });

//   it("createUser test", async () => {
//     const result = await createUser({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("createUser test", async () => {
//     const result = await createUser({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("updateAccStatus test", async () => {
//     const result = await updateAccStatus({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("updateAccDetails test", async () => {
//     const result = await updateAccDetails({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getRoleRealTime test", async () => {
//     const result = await getRoleRealTime({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("queryDefaultWindow test", async () => {
//     const result = await queryDefaultWindow({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("queryEditGroup test", async () => {
//     const result = await queryEditGroup({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("queryLeaderList test", async () => {
//     const result = await queryLeaderList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("queryUserPermission test", async () => {
//     const result = await queryUserPermission({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("queryEditUserPermission test", async () => {
//     const result = await queryEditUserPermission({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("queryUserInfo test", async () => {
//     const result = await queryUserInfo({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("queryUserList test", async () => {
//     const result = await queryUserList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });
// });

// describe("account list api test with refresh token test", () => {
//   beforeAll(async () => {
//     // Mock access token into session storage.
//     Object.defineProperty(window, "sessionStorage", {
//       value: {
//         access_token: `${ACCESS_TOKEN}`,
//       },
//       writable: true,
//     });

//     const result = await refreshToken();

//     // update refresh access token into session storage.
//     window.sessionStorage.access_token = `${result.data.result.new_access_token}`;
//   });

//   afterAll(() => {
//     window.sessionStorage.access_token = "";
//   });

//   it("createUser with empty data test", async () => {
//     const response = await createUser({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("updateAccStatus test", async () => {
//     const response = await updateAccStatus({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("updateAccDetails test", async () => {
//     const response = await updateAccDetails({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getRoleRealTime test", async () => {
//     const response = await getRoleRealTime({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("queryDefaultWindow test", async () => {
//     const response = await queryDefaultWindow({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("queryEditGroup test", async () => {
//     const response = await queryEditGroup({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("queryLeaderList test", async () => {
//     const response = await queryLeaderList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response.data.result).not.toHaveLength(0);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("queryUserPermission test", async () => {
//     const response = await queryUserPermission({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response.data.result).not.toHaveLength(0);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("queryEditUserPermission test", async () => {
//     const response = await queryEditUserPermission({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response.data.result).not.toHaveLength(0);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("queryUserInfo test", async () => {
//     const response = await queryUserInfo({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//     expect(response.data.result).toHaveProperty("name", "");
//     expect(response.data.result).toHaveProperty("email", "");
//     expect(response.data.result).toHaveProperty("created_time", "");
//     expect(response.data.result).toHaveProperty("updated_time", "");
//     expect(response.data.result).toHaveProperty("last_login_time", "");
//     expect(response.data.result).toHaveProperty("status", 0);
//     expect(response.data.result).toHaveProperty("login_num", 0);
//     expect(response.data.result).toHaveProperty("api_token", "");
//     expect(response.data.result).toHaveProperty("is_admin", "");
//   });

//   it("queryUserList test", async () => {
//     const response = await queryUserList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//     expect(response).toHaveProperty("data.num");
//   });
// });
