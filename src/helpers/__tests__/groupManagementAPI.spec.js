// Test Example
import { describe, it, expect, beforeEach, beforeAll, afterAll } from "vitest";
// import { createGroup, checkGroupName, deleteGroupById, getFliterFunction, getGroupsList } from "../groupManagementAPI";
// import { refreshToken } from "../query-api";
// import { ACCESS_TOKEN } from "../../constants/system";

describe("group management api comming soon", () => {
  it("fake unit test", async () => {
    expect(1 + 1).toEqual(2);
  });
});

// 因為後端 API 出現問題無法進行整合測試
// describe("group management api test with 401 Unauthorized", () => {
//   beforeEach(() => {
//     Object.defineProperty(window, "location", {
//       value: {
//         href: "example.com",
//       },
//       writable: true,
//     });
//   });

//   it("createGroup test", async () => {
//     const result = await createGroup({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("checkGroupName test", async () => {
//     const result = await checkGroupName({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("deleteGroupById test", async () => {
//     const result = await deleteGroupById({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getFliterFunction test", async () => {
//     const result = await getFliterFunction({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getGroupsList test", async () => {
//     const result = await getGroupsList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });
// });

// describe("group management api test with refresh token test", () => {
//   beforeAll(async () => {
//     // Mock access token into session storage.
//     Object.defineProperty(window, "sessionStorage", {
//       value: {
//         access_token: `${ACCESS_TOKEN}`,
//       },
//       writable: true,
//     });

//     const result = await refreshToken();

//     // update refresh access token into session storage.
//     window.sessionStorage.access_token = `${result.data.result.new_access_token}`;
//   });

//   afterAll(() => {
//     window.sessionStorage.access_token = "";
//   });

//   it("createGroup test", async () => {
//     const response = await createGroup({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("checkGroupName test", async () => {
//     const response = await checkGroupName({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("deleteGroupById test", async () => {
//     const response = await deleteGroupById({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getFliterFunction test", async () => {
//     const response = await getFliterFunction({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response.data.result).not.toHaveLength(0);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getGroupsList test", async () => {
//     const response = await getGroupsList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response.data.result).toHaveLength(0);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//     expect(response).toHaveProperty("data.num");
//   });
// });
