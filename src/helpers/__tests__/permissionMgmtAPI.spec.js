// Test Example
import { describe, it, expect, beforeEach, beforeAll, afterAll } from "vitest";
// import { createPermission, deletePermission, updatePermission, queryAllPermission, queryPermissionById, queryPermissionList } from "../permissionMgmtAPI";
// import { refreshToken } from "../query-api";
// import { ACCESS_TOKEN } from "../../constants/system";

describe("permission management api comming soon", () => {
  it("fake unit test", async () => {
    expect(1 + 1).toEqual(2);
  });
});

// 因為後端 API 出現問題無法進行整合測試
// describe("permission management api test with 401 Unauthorized", () => {
//   beforeEach(() => {
//     Object.defineProperty(window, "location", {
//       value: {
//         href: "example.com",
//       },
//       writable: true,
//     });
//   });

//   it("createPermission test", async () => {
//     const result = await createPermission({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("deletePermission test", async () => {
//     const result = await deletePermission({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("updatePermission test", async () => {
//     const result = await updatePermission({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("queryAllPermission test", async () => {
//     const result = await queryAllPermission({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("queryPermissionById test", async () => {
//     const result = await queryPermissionById({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("queryPermissionList test", async () => {
//     const result = await queryPermissionList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });
// });

// describe("permission management api with refresh token test", () => {
//   beforeAll(async () => {
//     // Mock access token into session storage.
//     Object.defineProperty(window, "sessionStorage", {
//       value: {
//         access_token: `${ACCESS_TOKEN}`,
//       },
//       writable: true,
//     });

//     const result = await refreshToken();

//     // update refresh access token into session storage.
//     window.sessionStorage.access_token = `${result.data.result.new_access_token}`;
//   });

//   afterAll(() => {
//     window.sessionStorage.access_token = "";
//   });

//   it("createPermission test", async () => {
//     const response = await createPermission({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("deletePermission test", async () => {
//     const response = await deletePermission({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("updatePermission test", async () => {
//     const response = await updatePermission({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("queryAllPermission test", async () => {
//     const response = await queryAllPermission({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//     expect(response.data.result).not.toHaveLength(0);
//   });

//   it("queryPermissionById test", async () => {
//     const response = await queryPermissionById({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//     expect(response).toHaveProperty("data.result.func_group_list");
//     expect(response).toHaveProperty("data.result.role_name");
//     expect(response.data.result.func_group_list).toHaveLength(0);
//   });

//   it("queryPermissionList test", async () => {
//     const response = await queryPermissionList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//     expect(response).toHaveProperty("data.num");
//     expect(response.data.result).toHaveLength(0);
//   });
// });
