// Test Example
import { describe, it, expect, beforeEach, beforeAll, afterAll } from "vitest";
// import { createData, checkRepeat, removeData, updateData, queryDetails, getAll, getGroupList } from "../redirectRulesAPI";
// import { refreshToken } from "../query-api";
// import { ACCESS_TOKEN } from "../../constants/system";

describe("redirect rules api comming soon", () => {
  it("fake unit test", async () => {
    expect(1 + 1).toEqual(2);
  });
});

// 因為後端 API 出現問題無法進行整合測試
// describe("redirect rules api test with 401 Unauthorized", () => {
//   beforeEach(() => {
//     Object.defineProperty(window, "location", {
//       value: {
//         href: "example.com",
//       },
//       writable: true,
//     });
//   });

//   it("createData test", async () => {
//     const result = await createData({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("checkRepeat test", async () => {
//     const result = await checkRepeat({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("removeData test", async () => {
//     const result = await removeData({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("updateData test", async () => {
//     const result = await updateData({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("queryDetails test", async () => {
//     const result = await queryDetails({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getAll test", async () => {
//     const result = await getAll({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getGroupList test", async () => {
//     const result = await getGroupList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });
// });

// describe("redirect rules api with refresh token test", () => {
//   beforeAll(async () => {
//     // Mock access token into session storage.
//     Object.defineProperty(window, "sessionStorage", {
//       value: {
//         access_token: `${ACCESS_TOKEN}`,
//       },
//       writable: true,
//     });

//     const result = await refreshToken();

//     // update refresh access token into session storage.
//     window.sessionStorage.access_token = `${result.data.result.new_access_token}`;
//   });

//   afterAll(() => {
//     window.sessionStorage.access_token = "";
//   });

//   it("createData test", async () => {
//     const response = await createData({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("checkRepeat test", async () => {
//     const response = await checkRepeat({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//     expect(response).toHaveProperty("data.result.repeat");
//     expect(response).toHaveProperty("data.result.repeat_type");
//   });

//   it("removeData test", async () => {
//     const response = await removeData({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("updateData test", async () => {
//     const response = await updateData({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("queryDetails test", async () => {
//     const response = await queryDetails({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//     expect(response.data.result).toHaveProperty("redirect_type", 0);
//     expect(response.data.result).toHaveProperty("group", 0);
//     expect(response.data.result).toHaveProperty("name", "");
//     expect(response.data.result).toHaveProperty("service_name", "");
//     expect(response.data.result).toHaveProperty("domain", "");
//     expect(response.data.result).toHaveProperty("adv_flag", 0);
//     expect(response.data.result).toHaveProperty("redirect_link", "");
//     expect(response.data.result).toHaveProperty("removable", false);
//   });

//   it("getAll test", async () => {
//     const response = await getAll({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//     expect(response).toHaveProperty("data.num");
//     expect(response.data.result).toHaveLength(0);
//   });

//   it("getGroupList test", async () => {
//     const response = await getGroupList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//     expect(response.data.result).not.toHaveLength(0);
//   });
// });
