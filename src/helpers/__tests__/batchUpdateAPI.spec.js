// Test Example
import { describe, it, expect, beforeEach, beforeAll, afterAll } from "vitest";
// import { updateTargetUrl, getTargetUrlList, queryTargetUrlList } from "../batchUpdateAPI";
// import { refreshToken } from "../query-api";
// import { ACCESS_TOKEN } from "../../constants/system";

describe("batch update api comming soon", () => {
  it("fake unit test", async () => {
    expect(1 + 1).toEqual(2);
  });
});

// 因為後端 API 出現問題無法進行整合測試
// describe("batch update api test with 401 Unauthorized", () => {
//   beforeEach(() => {
//     Object.defineProperty(window, "location", {
//       value: {
//         href: "example.com",
//       },
//       writable: true,
//     });
//   });

//   it("updateTargetUrl test", async () => {
//     const result = await updateTargetUrl({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getTargetUrlList test", async () => {
//     const result = await getTargetUrlList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("queryTargetUrlList test", async () => {
//     const result = await queryTargetUrlList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });
// });

// describe("batch update api test with refresh token test", () => {
//   beforeAll(async () => {
//     // Mock access token into session storage.
//     Object.defineProperty(window, "sessionStorage", {
//       value: {
//         access_token: `${ACCESS_TOKEN}`,
//       },
//       writable: true,
//     });

//     const result = await refreshToken();

//     // update refresh access token into session storage.
//     window.sessionStorage.access_token = `${result.data.result.new_access_token}`;
//   });

//   afterAll(() => {
//     window.sessionStorage.access_token = "";
//   });

//   it("updateTargetUrl test", async () => {
//     const response = await updateTargetUrl({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getTargetUrlList test", async () => {
//     const response = await getTargetUrlList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("queryTargetUrlList test", async () => {
//     const response = await queryTargetUrlList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });
// });
