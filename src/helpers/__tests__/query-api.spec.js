// Test Example
import { describe, it, expect, beforeEach, afterEach, beforeAll, afterAll } from "vitest";
// import { loginRequest, sidebarItemRequest, getSyetemVersion, errorReport, refreshToken } from "../query-api";
// import { ACCESS_TOKEN } from "../../constants/system";

describe("basic api with empty token test comming soon", () => {
  it("fake unit test", async () => {
    expect(1 + 1).toEqual(2);
  });
});

// 因為後端 API 出現問題無法進行整合測試
// describe("basic api with empty token test", () => {
//   beforeEach(() => {
//     Object.defineProperty(window, "location", {
//       value: {
//         href: "example.com",
//       },
//       writable: true,
//     });
//   });

//   it("loginRequest test", async () => {
//     const expectedResult = {
//       status: 200,
//       data: {
//         status: { return_code: "", message: "" },
//         access_token: "",
//         token_type: "",
//         account: "",
//         email: "",
//         user_id: 0,
//         is_admin: "",
//         default_func_sn: "",
//         picture_url: "",
//         error: "",
//       },
//     };
//     const result = await loginRequest({});
//     expect(result).toEqual(expectedResult);
//   });

//   it("sidebarItemRequest test", async () => {
//     const result = await sidebarItemRequest({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getSyetemVersion test", async () => {
//     const result = await getSyetemVersion({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("errorReport test", async () => {
//     const result = await errorReport({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });
// });

// describe("refresh token test", () => {
//   beforeEach(() => {
//     Object.defineProperty(window, "sessionStorage", {
//       value: {
//         access_token: `${ACCESS_TOKEN}`,
//       },
//       writable: true,
//     });
//   });

//   it("refresh token test", async () => {
//     const result = await refreshToken();

//     expect(result).toHaveProperty("data");
//     expect(result).toHaveProperty("data.result");
//     expect(result).toHaveProperty("data.result.new_access_token");
//     expect(result).toHaveProperty("data.result.status");
//     expect(result).toHaveProperty("status");
//   });

//   afterEach(() => {
//     window.sessionStorage.access_token = "";
//   });
// });

// describe("basic api with refresh token test", () => {
//   beforeAll(async () => {
//     // Mock access token into session storage.
//     Object.defineProperty(window, "sessionStorage", {
//       value: {
//         access_token: `${ACCESS_TOKEN}`,
//       },
//       writable: true,
//     });

//     const result = await refreshToken();

//     // update refresh access token into session storage.
//     window.sessionStorage.access_token = `${result.data.result.new_access_token}`;
//   });

//   afterAll(() => {
//     window.sessionStorage.access_token = "";
//   });

//   it("sidebarItemRequest test", async () => {
//     const result = await sidebarItemRequest({});

//     expect(result).toHaveProperty("data");
//     expect(result).toHaveProperty("status");
//   });

//   it("getSyetemVersion test", async () => {
//     const result = await getSyetemVersion({});

//     expect(result).toHaveProperty("data.result");
//     expect(result).toHaveProperty("data.status");
//     expect(result).toHaveProperty("status");
//   });

//   it("errorReport test", async () => {
//     const result = await errorReport({
//       content: "API testing",
//       type: "2",
//     });

//     expect(result).toHaveProperty("data");
//     expect(result).toHaveProperty("status");
//   });
// });
