// Test Example
import { describe, it, expect, beforeEach, beforeAll, afterAll } from "vitest";
// import { getDropdownList, getStatisticalData, getTrendData } from "../effectivenessAnalysisAPI";
// import { refreshToken } from "../query-api";
// import { ACCESS_TOKEN } from "../../constants/system";

describe("effectiveness analysis api comming soon", () => {
  it("fake unit test", async () => {
    expect(1 + 1).toEqual(2);
  });
});

// 因為後端 API 出現問題無法進行整合測試
// describe("effectiveness analysis api test with 401 Unauthorized", () => {
//   beforeEach(() => {
//     Object.defineProperty(window, "location", {
//       value: {
//         href: "example.com",
//       },
//       writable: true,
//     });
//   });

//   it("getDropdownList test", async () => {
//     const result = await getDropdownList({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getStatisticalData test", async () => {
//     const result = await getStatisticalData({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });

//   it("getTrendData test", async () => {
//     const result = await getTrendData({});
//     expect(result).toEqual("Error 401 Unauthorized");
//   });
// });

// describe("batch update api test with refresh token test", () => {
//   beforeAll(async () => {
//     // Mock access token into session storage.
//     Object.defineProperty(window, "sessionStorage", {
//       value: {
//         access_token: `${ACCESS_TOKEN}`,
//       },
//       writable: true,
//     });

//     const result = await refreshToken();

//     // update refresh access token into session storage.
//     window.sessionStorage.access_token = `${result.data.result.new_access_token}`;
//   });

//   afterAll(() => {
//     window.sessionStorage.access_token = "";
//   });

//   it("getDropdownList test", async () => {
//     const response = await getDropdownList({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getStatisticalData test", async () => {
//     const response = await getStatisticalData({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });

//   it("getTrendData test", async () => {
//     const response = await getTrendData({});
//     expect(response).toHaveProperty("status");
//     expect(response.status).toEqual(200);
//     expect(response).toHaveProperty("data");
//     expect(response).toHaveProperty("data.result");
//   });
// });
