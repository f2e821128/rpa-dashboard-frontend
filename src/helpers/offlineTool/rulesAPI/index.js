import { request } from "../../httpRequest";

// 線下工具-權限管理：新增權限
export const otCreateRule = (data) => {
  return request("POST", "api/offline_tool/add/rule", data);
};

// 線下工具-權限管理：刪除權限
export const otDeleteRule = (data) => {
  return request("POST", "api/offline_tool/delete/rule", data);
};

// 線下工具-權限管理：取得權限列表
export const otGetRuleList = (data) => {
  return request("POST", "api/offline_tool/get/rule_list", data);
};

// 線下工具-權限管理：取得功能下拉選單
export const otGetOnlyModuleData = (data) => {
  return request("POST", "api/offline_tool/get/only_module_drop_data", data);
};
