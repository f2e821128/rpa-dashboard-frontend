import { request } from "../../httpRequest";

// 線下工具-管理平台：新增使用者（認證碼）
export const otCreateUser = (data) => {
  return request("POST", "api/offline_tool/add/user", data);
};

// 線下工具-管理平台：編輯使用者（認證碼）啟用狀態、有效狀態
export const otUpdateUserStatus = (data) => {
  return request("POST", "api/offline_tool/edit/user_status", data);
};

// 線下工具-管理平台：取得使用者（認證碼）清單
export const otGetUserList = (data) => {
  return request("POST", "api/offline_tool/get/user_list", data);
};

// 線下工具-管理平台：取得使用者（認證碼）編輯資料
export const otGetUserEditData = (data) => {
  return request("POST", "api/offline_tool/get/user_edit_data", data);
};

// 線下工具-管理平台：儲存編輯使用者（認證碼）
export const otUpdateUserEditData = (data) => {
  return request("POST", "api/offline_tool/edit/user_edit_data", data);
};

// 線下工具-管理平台：取得功能下拉選單資料
export const otGetModuleDropData = (data) => {
  return request("POST", "api/offline_tool/get/module_drop_data", null);
};
