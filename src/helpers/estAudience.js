import axios from "axios";
import { logging } from "../utils/logging";

const DOMAIN =
  import.meta.env.VITE_ENV === "dev"
    ? import.meta.env.VITE_DEV_IP
    : import.meta.env.VITE_ENV === "devqa"
    ? import.meta.env.VITE_DEVQA_IP
    : import.meta.env.VITE_ENV === "staging"
    ? import.meta.env.VITE_STAGING_IP
    : import.meta.env.VITE_PROD_IP;

const instance = axios.create({
  baseURL: DOMAIN,
  headers: {
    "Content-Type": "application/json",
  },
  timeout: 10000,
});

instance.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

instance.interceptors.response.use(
  function (response) {
    // Do something with response data
    return { status: response.status, data: response.data };
  },
  function (error) {
    if (error.response) {
      switch (error.response.status) {
        case 401:
        // window.sessionStorage.clear();
        // window.location.href = "/home";
        // return "Error 401 Unauthorized";
        case 404:
          // go to 404 page
          return "Error 404 Not Found";
        case 500:
          // go to 500 page
          return "Error 500 Internal Server Error";
        default:
          logging(error.message, "error");
      }
    }
    if (!window.navigator.onLine) {
      return "ERR_INTERNET_DISCONNECTED";
    }
    return Promise.reject(error);
  }
);

function request(method, url, data = null, config) {
  method = method.toLowerCase();
  switch (method) {
    case "post":
      return instance.post(url, data, config);
    default:
      return false;
  }
}

export const checkEstAudience = (data) => {
  return request("POST", "", data);
};
