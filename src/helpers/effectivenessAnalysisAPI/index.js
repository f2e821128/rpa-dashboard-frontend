import { request } from "../httpRequest";

// 短網址：成效分析 - 取得成效分析下拉選單資料 API
export const getDropdownList = (data) => {
  return request("POST", "api/shortlink/get/sl_levels", data);
};

// 短網址：成效分析 - 取得統計分析資料 API
export const getStatisticalData = (data) => {
  return request("POST", "api/shortlink/get/sl_statistical_analysis", data);
};

// 短網址：成效分析 - 取得趨勢分析資料 API
export const getTrendData = (data) => {
  return request("POST", "api/shortlink/get/sl_trend_analysis", data);
};
