import { request, formDataRequest } from "../httpRequest";

// 短網址：新增網址資料 API
export const createNewKeyMapping = (data) => {
  return formDataRequest("POST", "api/shortlink/add/new_key_mapping", data);
};

// 短網址：產生短網址 QRcode 圖片 API
export const createQRcode = (data) => {
  return request("POST", "api/shortlink/add/qrcode_img", data);
};

// 短網址：產生短網址 API
export const createShortURL = (data) => {
  return request("POST", "api/shortlink/add/short_url", data);
};

// 短網址：新增短網址群組 API
export const createGroups = (data) => {
  return request("POST", "api/shortlink/add/sl_group", data);
};

// 短網址：檢查群組名稱是否重複 API
export const checkNameRepeat = (data) => {
  return request("POST", "api/shortlink/check/sl_group_name", data);
};

// 短網址：刪除網址資料 API
export const deleteKeyMapping = (data) => {
  return request("POST", "api/shortlink/delete/key_mapping", data);
};

// 短網址：刪除群組 API
export const deleteGroup = (data) => {
  return request("POST", "api/shortlink/delete/sl_group", data);
};

/**
 * 短網址：儲存網址編輯資料
 * 說明：更新編輯後的資料 API
 * @param {*} data
 * @returns
 */
export const updateShortURLDetails = (data) => {
  return request("POST", "api/shortlink/edit/mapping_edit", data);
};

/**
 * 短網址：儲存短網址編輯資料（備註）
 * 說明：更新短網址備註 API
 * @param {*} data
 * @returns
 */
export const updateShortURLRemark = (data) => {
  return request("POST", "api/shortlink/edit/short_url_edit", data);
};

/**
 * 短網址：編輯短網址
 * 說明：更新短網址群組 API
 * @param {*} data
 * @returns
 */
export const updateGroups = (data) => {
  return request("POST", "api/shortlink/edit/sl_group", data);
};

/**
 * 短網址：取得網址編輯資料
 * 說明：獲取短網址編輯資料 API
 * @param {task_id : string} data
 * @returns
 */
export const getShortURLDetails = (data) => {
  return request("POST", "api/shortlink/get/mapping_edit", data);
};

/**
 * 短網址：取得網址清單
 * 說明：獲取短網址列表 API
 * @param { page_num: number, page_size: number, scenario_id: string, search_txt: string, type: number } data
 * @returns
 */
export const getShortURLList = (data) => {
  return request("POST", "api/shortlink/get/mapping_list", data);
};

/**
 * 短網址：取得網址細項（短網址清單）
 * 說明：獲取短網址列表 API
 * @param { page_num: number, page_size: numbet, task_id": string } data
 * @returns
 */
export const getShortURLDetailsInfo = (data) => {
  return request("POST", "api/shortlink/get/mapping_sub_info", data);
};

/**
 * 短網址：取得短網址編輯資料（備註）
 * 說明：獲取短網址備註資訊 API
 * @param { short_url: string, task_id: string } data
 * @returns
 */
export const getShortURLRemark = (data) => {
  return request("POST", "api/shortlink/get/short_url_edit", data);
};

/**
 * 短網址：取得緩調規則（依照使用者選的群組組別篩選）
 * 說明：獲取專挑規則 API
 * @param { group: number } data
 * @returns
 */
export const getRedirectRules = (data) => {
  return request("POST", "api/shortlink/get/user_jump_rules", data);
};

/**
 * 短網址：取得使用者短網址組別清單
 * 說明：獲取使用者短網址組別清單 API
 * @param {*} data
 * @returns
 */
export const getGroupListByUser = (data) => {
  return request("POST", "api/shortlink/get/user_rpa_sl_group_list", data);
};

/**
 * 短網址：取得使用者短網址群組
 * 說明：網址清單頁面 API - 群組清單
 * @param {*} data
 * @returns
 */
export const getAvailableGroups = (data) => {
  return request("POST", "api/shortlink/get/user_sl_group_list", data);
};

/**
 * 短網址：重產短網址 API
 * @param {page_num: number, page_size: number, task_id: string } data
 * @returns
 */
export const regenerateShortURL = (data) => {
  return request("POST", "api/shortlink/recreate/short_url", data);
};
