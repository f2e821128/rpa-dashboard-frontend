/**
 * parse response
 */
function parseBody(response) {
  //  if (response.status === 200 && response.data.status.code === 200) { // - if use custom status code
  if (response.status === 200) {
    return response.data.result;
  } else {
    return this.parseError(response.data.messages);
  }
}

export default parseBody;
