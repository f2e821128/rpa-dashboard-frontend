import { request } from "../httpRequest";

// 短網址：轉跳規則頁面 API
export const createData = (data) => {
  return request("POST", "api/shortlink/add/new_jump_rule", data);
};

export const checkRepeat = (data) => {
  return request("POST", "api/shortlink/check/jump_rule", data);
};

export const removeData = (data) => {
  return request("POST", "api/shortlink/delete/jump_rule", data);
};

export const updateData = (data) => {
  return request("POST", "api/shortlink/edit/jump_rule_edit", data);
};

export const queryDetails = (data) => {
  return request("POST", "api/shortlink/get/jump_rule_edit_data", data);
};

export const getAll = (data) => {
  return request("POST", "api/shortlink/get/jump_rule_list", data);
};

export const getGroupList = () => {
  return request("POST", "api/shortlink/get/rpa_group_list", {});
};
