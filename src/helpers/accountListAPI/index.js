import { request } from "../httpRequest";

// 使用者管理：新增使用者
export const createUser = (data) => {
  return request("POST", "api/system/add/new_user", data);
};

// 使用者管理：更新帳號狀態
export const updateAccStatus = (data) => {
  return request("POST", "api/system/edit/change_user_status", data);
};

// 使用者管理：更新帳號資訊
export const updateAccDetails = (data) => {
  return request("POST", "api/system/edit/user_edit", data);
};

// 使用者管理：取得帳戶權限異動後相關資料
export const getRoleRealTime = (data) => {
  return request("POST", "api/system/get/data_about_change_role", data);
};

// 使用者管理：取得要編輯的使用者預設開啟畫面
export const queryDefaultWindow = (data) => {
  return request("POST", "api/system/get/default_open_with_user_dw", data);
};

// 使用者管理：取得要編輯的使用者組別
export const queryEditGroup = (data) => {
  return request("POST", "api/system/get/func_list_with_user_dw", data);
};

// 使用者管理：取得主管列表資料
export const queryLeaderList = (data) => {
  return request("POST", "api/system/get/leader_list", data);
};

// 使用者管理：取得帳號權限
export const queryUserPermission = (data) => {
  return request("POST", "api/system/get/role_list_dw", data);
};

// 使用者管理：取得要編輯的使用者權限
export const queryEditUserPermission = (data) => {
  return request("POST", "api/system/get/role_list_with_user_dw", data);
};

// 使用者管理：取得要編輯的使用者資料
export const queryUserInfo = (data) => {
  return request("POST", "api/system/get/user_info_edit", data);
};

// 使用者管理：取得使用者列表
export const queryUserList = (data) => {
  return request("POST", "api/system/get/user_list", data);
};
