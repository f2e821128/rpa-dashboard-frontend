import { request } from "../../httpRequest";

// 站群推播-標籤管理：新增標籤
export const createLabelTag = (data) => {
  return request("POST", "api/webpush/add/new_tag", data);
};

// 站群推播-標籤管理：檢查新增標籤
export const checkNewLabelTag = (data) => {
  return request("POST", "api/webpush/check/tag_name_topic", data);
};

// 站群推播-標籤管理：刪除標籤
export const deleteLabelTag = (data) => {
  return request("POST", "api/webpush/delete/tag", data);
};

// 站群推播-標籤管理：取得標籤清單
export const getLabelTag = (data) => {
  return request("POST", "api/webpush/get/tag_list", data);
};

// 站群推播-標籤管理：取得標籤類型å清單
export const getLabelTagType = (data) => {
  return request("POST", "api/webpush/get/tag_topic_list", data);
};
