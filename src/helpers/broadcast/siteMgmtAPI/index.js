import { request } from "../../httpRequest";

// 站群推播-網站管理：新增網站
export const createSite = (data) => {
  return request("POST", "api/webpush/add/new_website", data);
};

// 站群推播-網站管理：檢查網站名稱是否重複
export const checkSiteName = (data) => {
  return request("POST", "api/webpush/check/website_name", data);
};

// 站群推播-網站管理：刪除網站
export const deleteSite = (data) => {
  return request("POST", "api/webpush/delete/website", data);
};

// 站群推播-網站管理：儲存網站編輯的資料
export const updateSiteInfo = (data) => {
  return request("POST", "api/webpush/edit/website_edit", data);
};

// 站群推播-網站管理：儲存網站群組編輯
export const updateSiteGroup = (data) => {
  return request("POST", "api/webpush/edit/website_group_edit", data);
};

// 站群推播-網站管理：取得網站歸戶下拉資料
export const getDomainList = (data) => {
  return request("POST", "api/webpush/get/group_domain_list", data);
};

// 站群推播-網站管理：取得娛樂城下拉資料
export const getEntmtCityList = (data) => {
  return request("POST", "api/webpush/get/prefix_list", data);
};

// 站群推播-網站管理：取得使用者有權利的站群推播組別清單
export const getBroadcastGroupList = (data) => {
  return request("POST", "api/webpush/get/rpa_group_list", data);
};

// 站群推播-網站管理：取得網站下拉資料
export const getSiteList = (data) => {
  return request("POST", "api/webpush/get/site_prefix_list", data);
};

// 站群推播-網站管理：取得網站下拉資料
export const getWebsiteData = (data) => {
  return request("POST", "api/webpush/get/website_data", data);
};

// 站群推播-網站管理：取得群組下拉資料
export const getGroupList = (data) => {
  return request("POST", "api/webpush/get/website_dw_group_list", data);
};

// 站群推播-網站管理：取得成效分析資料
export const geteffectivenessAnalysis = (data) => {
  return request("POST", "api/webpush/get/website_eff_analysis", data);
};

// 站群推播-網站管理：取得網站群組編輯資料
export const getWebsiteGroupData = (data) => {
  return request("POST", "api/webpush/get/website_group_data", data);
};

// 站群推播-網站管理：取得查詢群組清單
export const getWebsiteGroupList = (data) => {
  return request("POST", "api/webpush/get/website_group_list", data);
};

// 站群推播-網站管理：取得查詢網站清單
export const getWebsiteList = (data) => {
  return request("POST", "api/webpush/get/website_list", data);
};

// 站群推播-網站管理：取得群組內網站名稱清單（成效分析下拉選單用）
export const getSiteListByGroup = (data) => {
  return request("POST", "api/webpush/get/website_list_by_group", data);
};

// 站群推播-網站管理：取得使用者有權限的標籤清單
export const getTagList = (data) => {
  return request("POST", "api/webpush/get/website_tag_list", data);
};
