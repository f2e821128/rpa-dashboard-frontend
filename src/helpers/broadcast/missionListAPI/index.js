import { request, formDataRequest } from "../../httpRequest";

// 站群推播-任務清單：新增任務
export const createSite = (data) => {
  return request("POST", "api/webpush/add/new_mission", data);
};

// 站群推播-任務清單：檢查是否有重複的推播開始時間
export const checkBeginTimeRepeat = (data) => {
  return request("POST", "api/webpush/check/begin_time", data);
};

// 站群推播-任務清單：檢查相同推播模組，是否推給相同推播目標
export const checkJobRepeat = (data) => {
  return request("POST", "api/webpush/check/repeat", data);
};

// 站群推播-任務清單：刪除任務
export const deleteMissionData = (data) => {
  return request("POST", "api/webpush/delete/mission_data", data);
};

// 站群推播-任務清單：儲存編輯任務資料
export const updateMissionData = (data) => {
  return request("POST", "api/webpush/edit/mission_data", data);
};

// 站群推播-任務清單：上傳圖片，取得 GCS 圖片 URL
export const uploadImage = (data) => {
  return formDataRequest("POST", "api/webpush/get/broadcast_upload_img", data);
};

// 站群推播-任務清單：取得編輯任務資料
export const getMissionData = (data) => {
  return request("POST", "api/webpush/get/edit_mission_data", data);
};

// 站群推播-任務清單：取得會員娛樂城下拉資料
export const getEntmtCityLists = (data) => {
  return request("POST", "api/webpush/get/member_prefix_list", data);
};

// 站群推播-任務清單：取得會員網站下拉資料
export const getSiteMemberList = (data) => {
  return request("POST", "api/webpush/get/member_site_prefix_list", data);
};

// 站群推播-任務清單：取得建立者、推播標籤、推播目標資料
export const getMisiionInfo = (data) => {
  return request("POST", "api/webpush/get/mission_data", data);
};

// 站群推播-任務清單：取得任務清單列表
export const getMissionDataList = (data) => {
  return request("POST", "api/webpush/get/mission_list", data);
};

// 站群推播-任務清單-成效分析：取得推播追蹤圖表資料
export const getAnalChartData = (data) => {
  return request("POST", "api/webpush/get/get_mission_eff_chart_data", data);
};

// 站群推播-任務清單-成效分析：取得推播分析 - 推播受眾數據表格資料
export const getBroadcastSheetData = (data) => {
  return request("POST", "api/webpush/get/get_mission_eff_table1_data", data);
};

// 站群推播-任務清單-成效分析：取得推播分析 - 推播網站數據表格資料
export const getBroadcastSiteData = (data) => {
  return request("POST", "api/webpush/get/get_mission_eff_table2_data", data);
};

// 站群推播-任務清單-成效分析：取得推播分析 - 推播活動廣告素材數據
export const getEventAdsData = (data) => {
  return request("POST", "api/webpush/get/get_mission_eff_table3_data", data);
};

// 站群推播-任務清單-成效分析：取得推播分析 - 會員成效 - ESB會員成效
export const getMemberEff = (data) => {
  return request("POST", "api/webpush/get/get_mission_eff_table4_member_data", data);
};

// 站群推播-任務清單-成效分析：取得成效分析任務詳情
export const getEffAnalMissionData = (data) => {
  return request("POST", "api/webpush/get/get_mission_info_for_eff", data);
};

// 站群推播-任務清單-成效分析：取得成效分析清單
export const getEffAnalList = (data) => {
  return request("POST", "api/webpush/get/get_mission_list_for_eff", data);
};

// 站群推播-任務清單-成效分析：取得任務清單下拉選單資料
export const getMissionList = (data) => {
  return request("POST", "api/webpush/get/mission_dw_list", data);
};

// 站群推播-任務清單-成效分析：取得推播標籤下拉選單資料
export const getBroadcastLabels = (data) => {
  return request("POST", "api/webpush/get/tag_dw_list", data);
};
