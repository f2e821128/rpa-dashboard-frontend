import axios from "axios";
import parseError from "./parseError";

const DOMAIN =
  import.meta.env.VITE_ENV === "dev"
    ? import.meta.env.VITE_DEV_IP
    : import.meta.env.VITE_ENV === "devqa"
    ? import.meta.env.VITE_DEVQA_IP
    : import.meta.env.VITE_ENV === "staging"
    ? import.meta.env.VITE_STAGING_IP
    : import.meta.env.VITE_PROD_IP;

const instance = axios.create({
  baseURL: DOMAIN,
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
  timeout: 10000,
});

instance.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    config.headers["Authorization"] = window.sessionStorage?.access_token;
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

instance.interceptors.response.use(
  function (response) {
    // Do something with response data
    return { status: response.status, data: response.data };
  },
  function (error) {
    if (error.response) {
      switch (error.response.status) {
        case 401:
        // 先清除 Token key 再重新導向
        // window.sessionStorage.clear();
        // window.location.href = "/home";
        // return "Error 401 Unauthorized";
        case 404:
          // go to 404 page
          return "Error 404 Not Found";
        case 500:
          // go to 500 page
          return "Error 404 Internal Server Error";
        default:
          return parseError(error.message);
      }
    }
    if (!window.navigator.onLine) {
      return "ERR_INTERNET_DISCONNECTED";
    }
    return Promise.reject(error);
  }
);

function request(method, url, data = null, config) {
  method = method.toLowerCase();
  switch (method) {
    case "post":
      return instance.post(url, data, config);
    case "get":
      return instance.get(url, { params: data });
    case "delete":
      return instance.delete(url, { params: data });
    case "put":
      return instance.put(url, data);
    case "patch":
      return instance.patch(url, data);
    default:
      return false;
  }
}

const formDataInstance = axios.create({
  baseURL: DOMAIN,
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-Type": "multipart/form-data",
  },
  timeout: 100000,
});

formDataInstance.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    config.headers["Authorization"] = window.sessionStorage?.access_token;
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

formDataInstance.interceptors.response.use(
  function (response) {
    // Do something with response data
    return { status: response.status, data: response.data };
  },
  function (error) {
    if (error.response) {
      switch (error.response.status) {
        case 401:
        // 先清除 Token key 再重新導向
        // window.sessionStorage.clear();
        // window.location.href = "/home";
        // return "Error 401 Unauthorized";
        case 404:
          // go to 404 page
          return "Error 404 Not Found";
        case 500:
          // go to 500 page
          return "Error 500 Internal Server Error";
        default:
          console.log(error.message);
      }
    }
    if (!window.navigator.onLine) {
      return "ERR_INTERNET_DISCONNECTED";
    }
    return Promise.reject(error);
  }
);

function formDataRequest(method, url, data = null, config) {
  method = method.toLowerCase();
  switch (method) {
    case "post":
      return formDataInstance.post(url, data, config);
    default:
      return false;
  }
}

export { request, formDataRequest };
