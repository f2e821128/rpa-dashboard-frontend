import { request } from "../httpRequest";

// 組別管理：新增組別 API
export const createGroup = (data) => {
  return request("POST", "api/system/add/rpa_group", data);
};

// 組別管理：檢查組別名稱 API
export const checkGroupName = (data) => {
  return request("POST", "api/system/check/rpa_group_name", data);
};

// 組別管理：刪除組別 API
export const deleteGroupById = (data) => {
  return request("POST", "api/system/delete/rpa_group", data);
};

// 組別管理：取得功能清單 API
export const getFliterFunction = (data) => {
  return request("POST", "api/system/get/main_func_list", data);
};

// 組別管理：取得組別列表 API
export const getGroupsList = (data) => {
  return request("POST", "api/system/get/rpa_group_list", data);
};
