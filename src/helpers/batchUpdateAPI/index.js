import { request } from "../httpRequest";

// 短網址：批次更新 - 批次更新目標網址
export const updateTargetUrl = (data) => {
  return request("POST", "api/shortlink/batch_update/target_url", data);
};

// 短網址：批次更新 - 取得目標網址相關網址清單
export const getTargetUrlList = (data) => {
  return request("POST", "api/shortlink/get/target_task_list", data);
};

// 短網址：批次更新 - 取得搜尋的網域名稱清單
export const queryTargetUrlList = (data) => {
  return request("POST", "api/shortlink/get/target_url_list", data);
};
