import { request } from "./httpRequest";

export const loginRequest = (googleCredential) => {
  return request("POST", "api/auth/login", { id_token: googleCredential });
};

export const sidebarItemRequest = () => {
  return request("POST", "api/other/get/menu_data", {});
};

export const getSyetemVersion = () => {
  return request("POST", "api/system/get/version", {});
};

export const errorReport = (errorMsg) => {
  return request("POST", "api/other/log/frontend_log", errorMsg);
};

export const refreshToken = () => {
  return request("POST", "api/other/recreate/jwt_token", {});
};
