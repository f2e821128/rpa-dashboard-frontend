import { request } from "../httpRequest";

// 權限管理: 新增權限
export const createPermission = (data) => {
  return request("POST", "api/system/add/new_role", data);
};

// 權限管理: 刪除權限
export const deletePermission = (data) => {
  return request("POST", "api/system/delete/role", data);
};

// 權限管理: 更新權限
export const updatePermission = (data) => {
  return request("POST", "api/system/edit/role_edit", data);
};

// 權限管理: 取得所有權限
export const queryAllPermission = (data) => {
  return request("POST", "api/system/get/fuc_authority", data);
};

// 權限管理: 更新權限
export const queryPermissionById = (data) => {
  return request("POST", "api/system/get/role_edit", data);
};

// 權限管理: 取得權限
export const queryPermissionList = (data) => {
  return request("POST", "api/system/get/role_list", data);
};
