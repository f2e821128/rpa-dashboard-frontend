import { createRouter, createWebHistory } from "vue-router";

const ifAuthenticated = (to, from, next) => {
  if (window.sessionStorage.getItem("googleCredential")) {
    next();
    return;
  }
  window.location.href = "/";
};

const routes = [
  {
    path: "/",
    component: () => import("../layouts/LoginLayout.vue"),
  },
  {
    path: "/home",
    component: () => import("../layouts/MainLayout.vue"),
    children: [{ path: "", component: () => import("../pages/IndexPage.vue") }],
    beforeEnter: ifAuthenticated,
  },
  {
    path: "/:catchAll(.*)*",
    component: () => import("../pages/ErrorNotFound.vue"),
  },
];

export default createRouter({
  history: createWebHistory(),
  routes,
});
