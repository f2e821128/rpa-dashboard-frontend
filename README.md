# RPA Platform

## 目的

舊的 RPA 平台上面有很多舊的系統和之前都是以工程師的角度去進行開發。
改版的原因：希望參評可以更加有使用者體驗，將 RPA 平台做到更像一套正真的產品。

## 範例

[RPA Dashboard](https://rpa-dashboard-f2e.netlify.app)

## Screenshot

![alt cover](/public/screenshot.jpg)

## 開發環境

- Node Version - v20.9.0
- NVM Version - v0.35.2
- NPM Version - v10.1.0
- YARN Version - v1.22.21

### 環境安裝

[安裝 Node 教學](https://www.jb51.net/article/283896.htm)
[安裝 nvm 教學](https://www.ruyut.com/2023/05/linux-nvm.html)

### 執行專案

```
# 安裝所需要的套件
yarn

# 在 Local 端執行
yarn dev
```

## Use Technology & Library

- Vue 3
- Tailwind CSS
- Axios
- highcharts-vue
- pinia
- vue-flatpickr-component
- vue-i18n
- vue-multiselect
- vue-router
- vue3-easy-data-table
- vue3-google-signin

## Features

- [x] 新增站群推播排程，包含（增、刪、改、查）功能
- [x] 站群推播網站管理介面
- [x] 站群推播，推播模組管理
- [x] 短網址管理介面，包含（增、刪、改、查）功能
- [x] 短網址轉跳規則管理介面
- [x] 使用者管理介面
- [x] 權限管理介面
- [x] 群組管理介面
