import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
// import VueI18nPlugin from "@intlify/unplugin-vue-i18n/vite";

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    headers: {
      "Strict-Transport-Security": "max-age=86400; includeSubDomains", // Adds HSTS options to your website, with a expiry time of 1 day
      "X-Content-Type-Options": "nosniff", // Protects from improper scripts runnings
      "X-Frame-Options": "DENY", // Stops your site being used as an iframe
      "X-XSS-Protection": "1; mode=block", // Gives XSS protection to legacy browsers
    },
  },
  plugins: [
    vue(),
    // VueI18nPlugin({
    //   /* options */
    //   // locale messages resource pre-compile option
    //   include: resolve(dirname(fileURLToPath(import.meta.url)), "./path/to/src/locales/**"),
    // }),
  ],
  define: {
    __APP_VERSION__: JSON.stringify(process.env.npm_package_version),
  },
  resolve: {
    alias: {
      "@": "/src",
    },
  },
  test: {
    environment: "jsdom",
    globals: false,
    setupFiles: "vitest-setup.js",
    coverage: {
      provider: "v8", // or 'istanbul'
    },
  },
});
