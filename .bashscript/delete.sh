# 刪除 Cloud Run Revision 的 Bash 檔案
# 獲取 Cloud Run 服務 Revision Name (Demo)
gcloud run revisions list --format="value(metadata.name)" --platform=managed --region=asia-northeast1 --service=demo-rpa-pltf-ap --sort-by="~metadata.creationTimestamp"

# 獲取 Cloud Run 服務 Revision Name (測試)
gcloud run revisions list --format="value(metadata.name)" --platform=managed --region=asia-northeast1 --service=dev-vite-rpa-platform --sort-by="~metadata.creationTimestamp"

# 獲取 Cloud Run 服務 Revision Name (正式)
gcloud run revisions list --format="value(metadata.name)" --platform=managed --region=asia-northeast1 --service=prd-rpa-pltf-ap --sort-by="~metadata.creationTimestamp"

# 刪除 Cloud Run Revision 指令
gcloud run revisions delete demo-rpa-pltf-ap-00014-wed --region=asia-northeast1 --quiet

# 最後，在 Terminal 上執行 sh delete.sh 即可