#!/bin/bash

git checkout Integration-Test
git merge feature
echo `date +"%Y-%m-%d %T merge into Integration-Test"` >> .logging/mergelog.txt
git add .
git commit -m "merge into Integration-Test"
git push -u origin Integration-Test

git checkout qa
git merge Integration-Test
echo `date +"%Y-%m-%d %T merge into qa"` >> .logging/mergelog.txt
git add .
git commit -m "merge into qa"
git push -u origin qa

git checkout prod
git merge qa
echo `date +"%Y-%m-%d %T merge into prod"` >> .logging/mergelog.txt
git add .
git commit -m "merge into prod"
git push -u origin prod

git checkout main
git merge prod
echo `date +"%Y-%m-%d %T merge into main"` >> .logging/mergelog.txt
git add .
git commit -m "merge into main"
git push -u origin main