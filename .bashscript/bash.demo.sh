#!/bin/bash

# Google artifacts registry region 設定
REGION=asia-northeast1-docker.pkg.dev

# Google Project ID
PROJECTID=

# Cloud Run Name
PROJECT=demo-rpa-pltf-ap

#部署環境 [讀取第一個參數]
BUILDENV=$1

#版本號碼 [讀取第二個參數]
VERSION=$2

if [ -z "$BUILDENV" ]; then
	echo "用法: sh build.sh [Arg1] [Arg2]"
	echo "Arg1: Build版環境(local/dev/demo/prod)"
	echo "Arg2: 版本號碼"
	exit 0
fi

if [ -z "$VERSION" ]; then
 	echo "\n沒有指定版本號碼"
 	exit 0
fi

echo "\n建立image檔案..."
docker system prune --force --volumes
docker build -t rpa-frontend-"$BUILDENV":"$VERSION" --build-arg ENV=dev .

if [ "$BUILDENV" != "local" ]; then
  # 將 Image 加上 tag
	echo "\n标记本地镜像"
	docker tag rpa-frontend-"$BUILDENV":"$VERSION" $REGION/$PROJECTID/rpa-frontend/rpa-frontend-"$BUILDENV":"$VERSION"

	# 將 Image 推到 artifacts registry 上
	echo "\n上傳至gcp..."
	docker push $REGION/$PROJECTID/rpa-frontend/rpa-frontend-"$BUILDENV":"$VERSION"

	# 部署
	echo "\n部署到 Cloud Run 上..."
	gcloud run deploy $PROJECT --image=$REGION/$PROJECTID/rpa-frontend/rpa-frontend-"$BUILDENV":"$VERSION" --platform=managed --region=asia-northeast1 --project=$PROJECTID
fi

echo "\nAll Done!!"