module.exports = {
  root: true,
  extends: ["plugin:vue/vue3-essential", "@vue/airbnb"],
  parserOptions: {
    ecmaVersion: "latest",
  },
};
